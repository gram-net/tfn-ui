/* eslint-disable prettier/prettier */
// https://nightwatchjs.org/api#expect-api
const timeout = 1000;
const addr = require('path').resolve(__dirname + '/assets/wallet1.addr');
const pk = require('path').resolve(__dirname + '/assets/wallet1.pk');

const elements = {
  page: ".main-content[data-loading=false]",
  nav: "[name=nav-drawer]",
  navButton: "[name=nav-wallet]",
  modal: "[name=modal-wallet]",
  close: "[name=modal-wallet] .base-dialog__header__close",
  name: "[name=wallet-name]",
  addr: "[name=wallet-addr]",
  addrButton: "[name=wallet-select-addr]",
  pk: "[name=wallet-pk]",
  pkButton: "[name=wallet-select-pk]",
  submit: "[name=wallet-submit]",
  error: ".v-messages__message:first-of-type",
  item: "[name=wallet-item]",
  itemAddr: "[name=wallet-item] [name=wallet-item-addr]",
  itemPk: "[name=wallet-item] [name=wallet-item-pk]",
  itemName: "[name=wallet-item] [name=wallet-item-name]",
  itemAmount: "[name=wallet-item] [name=wallet-item-amount]",
  itemDelete: "[name=wallet-item] [name=wallet-item-delete]",
}

module.exports = {
  ["Wallet Loader"](b) {
    return b.url("http://localhost:8080/#/tigerbee")
      .pause(2000)
      .waitForElementVisible(elements.page, 5000)
      .waitForElementVisible(elements.nav, timeout)
      .click(elements.nav)
      .pause(200)
      .waitForElementVisible(elements.navButton, 5000)
      .click(elements.navButton)
      .waitForElementPresent(elements.modal, timeout)
      .setValue(elements.pk, pk)
      .setValue(elements.addr, addr)
      .pause(1500)
      .assert.containsText(elements.addrButton, "WALLET1")
      .assert.containsText(elements.pkButton, "WALLET1")
      .setValue(elements.name, "B")
      .assert.containsText(elements.addrButton, "WALLET1B")
      .getAttribute(elements.addr, 'data-value', (data) => {
        const addr = data.value;
        b.click(elements.submit)
          .pause(1500)
          .waitForElementPresent(elements.item, timeout)
          .waitForElementPresent(elements.itemName, timeout)
          .waitForElementPresent(elements.itemAddr, timeout)
          .waitForElementPresent(elements.itemDelete, timeout)
          .click(elements.itemDelete)
          .waitForElementNotPresent(elements.item, timeout)
      })
      .click(elements.submit)
      .assert.containsText(elements.error, "Please fill out all fields")
      .pause(2000)
      .end()
  }
};
