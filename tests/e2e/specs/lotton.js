/* eslint-disable prettier/prettier */
// https://nightwatchjs.org/api#expect-api
const timeout = 1000;
const image = require('path').resolve(__dirname + '/assets/lotton-image.jpg');
let longString = '';
let newTicketId = '';
const loremIpsum = require("lorem-ipsum").loremIpsum;

while (longString.length < 255 || longString.length > 1024) {
  longString = loremIpsum({
    count: 30,
    units: "words"
  })
}

const elements = {
  navBack: "[name=topbar-back]",
  navLogo: "[name=topbar-logo]",
  navTickets: "[name=bottombar-tickets]",
  navWinners: "[name=bottombar-winners]",
  ticketId: "[name=ticket-id]",
  pages: {
    ticket: "[name=page-ticket]",
    winners: "[name=page-winners]",
    tickets: "[name=page-tickets]",
  },
  form: {
    form: "[name=form]",
    amount: "[name=form-amount] input",
    wallet: "[name=form-wallet]",
    walletDialog: "[name=form-wallet-dialog]",
    walletDialogItem: ".dialog-item:nth-child(2)",
    longString: "[name=form-long-string]",
    image: "[name=form-wallet-image]",
    thumb: "[name=form-wallet-thumb]",
    submit: "[name=form-submit]",
  },
  soldTicket: (id) => `[name=sold-ticket-${id}]`,
  winner: (id) => `[name=winner-${id}]`,
}

module.exports = {
  ["Buy Lotton Ticket"](b) {
    b.url("http://localhost:8080/#/lotton")
      .pause(2000)
      .waitForElementPresent(elements.pages.tickets, 5000)
      .waitForElementPresent(elements.form.amount, timeout)
      .waitForElementPresent(elements.form.submit, timeout)
      .setValue(elements.form.amount, 500)
      .click(elements.form.wallet)
      .waitForElementVisible(elements.form.walletDialog, timeout)
      .waitForElementPresent(elements.form.walletDialogItem, timeout)
      .click(elements.form.walletDialogItem)
      .waitForElementVisible(elements.form.longString, timeout)
      .setValue(elements.form.longString, longString)
      .assert.value(elements.form.longString, longString)
      .setValue(elements.form.image, image)
      .waitForElementVisible(elements.form.thumb, timeout)
      .click(elements.form.submit)
      .getAttribute(elements.form.form, 'data-last-ticket-id', (data) => {
        const id = data.value;
        const ticket = elements.soldTicket(id);
        b.waitForElementPresent(ticket, timeout)
          .click(ticket)
          .waitForElementPresent(elements.pages.ticket, 15000)
          .pause(100)
          .waitForElementPresent(elements.ticketId, timeout)
          .assert.containsText(elements.ticketId, id)
      })
      .click(elements.navWinners)
      .waitForElementPresent(elements.pages.winners, 15000)
      .waitForElementPresent(elements.navBack, timeout)
      .click(elements.navLogo)
      .waitForElementPresent(elements.pages.tickets, 15000)
      .waitForElementPresent(elements.form.form, timeout)
      .assert.value(elements.form.amount, "0")
      .assert.value(elements.form.longString, "")
      .click(elements.form.submit)
      .waitForElementPresent(".group-label__msg:first-of-type", timeout)
      .assert.containsText(".group-label__msg:first-of-type", "Amount is required")
      .pause(2000)
      .end()
  }
};
