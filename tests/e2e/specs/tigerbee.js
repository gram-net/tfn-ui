/* eslint-disable prettier/prettier */
// https://nightwatchjs.org/api#expect-api
const timeout = 1000;
const tapplet = require('path').resolve(__dirname + '/assets/roi-wc.zip');
const tapName = "roi-wc";
const fs = require("fs-extra");

const elements = {
  file: "[name=tapplet-file]",
  title: "[name=tapplet-title]",
  iframe: "[name=tapplet-iframe]",
  item: `[name=tapplet-item-${tapName}]`,
  itemDelete: `[name=tapplet-item-delete]`,
  itemMount: `[name=tapplet-item-mount]`,
  itemName: `[name=tapplet-item-name]`,
  itemUnmount: `[name=tapplet-item-unmount]`,
}

const clearTapplets = () => {
  const base = './public/tapplets/';
  const dir = fs.readdirSync(base);
  dir.forEach(async item => await fs.remove(base + item))
}

module.exports = {
  before() {
    clearTapplets();
  },
  ["Mount Tapplet"](b) {
    b.url("http://localhost:8080/#/tigerbee/tapplets")
      .pause(2000)
      .waitForElementPresent(elements.file, 5000)
      .waitForElementPresent(elements.title, timeout)
      .assert.containsText(elements.title, "Hi, welcome stranger")
      .pause(1500)
      .setValue(elements.file, tapplet)
      .waitForElementPresent(elements.item, timeout)
      .waitForElementPresent(elements.itemName, timeout)
      .assert.containsText(elements.itemName, tapName)
      .pause(10000)
      .click(elements.itemMount)
      // .waitForElementPresent(elements.iframe, 5000)
      .assert.containsText(elements.title, tapName)
      .click(elements.itemDelete)
      .pause(2000)
      .end()
  }
};
