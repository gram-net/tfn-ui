import { makeid } from "@/common/lib/makeid";

test("makeid", () => {
  const id = makeid(20);
  expect(id).toHaveLength(20);
  expect(typeof id).toBe("string");
});
