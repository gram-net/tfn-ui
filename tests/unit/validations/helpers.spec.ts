import { validEmail } from "../../../src/common/validations/helpers";

describe("validators", () => {
  describe("validEmail", () => {
    describe("with a valid email", () => {
      it("should return true", () => {
        expect(validEmail("a-valid@email.address")).toBeTruthy();
      });
    });
    describe("with a non valid email", () => {
      it("should return false", () => {
        expect(validEmail("not a valid email")).toBeFalsy();
      });
    });
  });
});
