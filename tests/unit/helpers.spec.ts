import { arrayToSentence, isJson } from "../../src/common/lib/helpers";

describe("helpers", () => {
  describe("arrayToSentence", () => {
    it("should convert an array of strings to a single sentence string", () => {
      const strArray = ["one", "two", "three", "four"];
      expect(arrayToSentence(strArray)).toBe("One, two, three and four");
    });
  });
  describe("isJson", () => {
    describe("with a valid JSON Object", () => {
      it("should return true", () => {
        expect(isJson({})).toBeTruthy();
        expect(isJson({ key: "value" })).toBeTruthy();
      });
    }),
      describe("with a non valid JSON Object", () => {
        it("should return false", () => {
          expect(isJson("not a JSON object")).toBeFalsy();
          expect(isJson(["another example"])).toBeFalsy();
        });
      });
  });
});
