import "../../../mocks/matchMedia";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import Console from "@/apps/tigerbee/containers/Console.vue";
import Vuex from "vuex";
import Vuetify from "vuetify";
import PortalVue from "portal-vue";
import { LogEntry } from "@/common/models/logentry";

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(Vuetify);
localVue.use(PortalVue);

describe("Console.vue", () => {
  let store;

  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        Console: {
          logs: <LogEntry[]>[]
        }
      },
      mutations: {
        "Common/stopLoading": () => {}
      }
    });
  });

  it("renders a default message when no logs are found", () => {
    const wrapper = shallowMount(Console, { store, localVue });
    expect(wrapper.text()).toMatch("No logs to show here! Move along.");
  });

  it("renders a log entry", () => {
    let logText = "this is a log entry";
    store.state.Console.logs = [new LogEntry([logText], "log")];
    const wrapper = shallowMount(Console, { store, localVue });
    expect(
      wrapper
        .get("span.linebreaks")
        .text()
        .trim()
    ).toEqual(logText);
  });
});
