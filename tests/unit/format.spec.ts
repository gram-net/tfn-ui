import { toFixed, abbrNumber } from "../../src/common/lib/format";

test("round fixed", () => {
  expect(toFixed(1.5555, 2)).toBe("1.56");
  expect(toFixed(1.9995)).toBe("2.00");
});

test("abbreviate number", () => {
  const kNum = 1450;
  const mStr = "1000000";
  const bStr = "1000000000";

  expect(abbrNumber(kNum)).toBe("1.4k");
  expect(abbrNumber(mStr)).toBe("1m");
  expect(abbrNumber(bStr)).toBe("1b");
});
