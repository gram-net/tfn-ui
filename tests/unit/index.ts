import Vue from "vue";
import Vuetify from "vuetify";
Vue.config.productionTip = false;
Vue.use(Vuetify); // https://github.com/vuetifyjs/vuetify/issues/4068
