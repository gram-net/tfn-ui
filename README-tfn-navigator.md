# TigerBee UI

## Getting Started

Refer to the [main README.md](https://gitlab.com/tigerbee/tfn-core/README.md)

### Run the setup script

`yarn setup`

### Connect to testnet

* run `rm -rf ../ton-remote && mkdir -p ../ton-remote && curl http://18.177.193.84:8084/liteserver.pub > ../ton-remote/liteserver.pub`
* `cp .env.testnet .env` to use the testnet config file

#### For custom testnets, replace the TON_IP variable in .env

18.177.193.84 is set for you, which is (at the time of this writing) TFN testnet-a master node TFN-API server

### Connect to local (or docker) API instance

`cp .env.local .env`

### Run the app

`yarn start`
`yarn ios`
`yarn android`
`yarn browser`

## Developer Guide

## Troubleshooting

### Clear local storage

Option 1: Open the navigation drawer, select 'Expert Mode', select 'Storage' from the Expert menu, and then select 'Clear' from the Storage screen

Option 2:  In the Electron Dev Tools UI, click console tab. at the bottom type in `window.localStorage.clear()` and press enter

### iOS

#### You must prepare cordova iOS simulator manually

```bash
cd src-cordova/platforms/ios/cordova && yarn add ios-sim@latest
cd ../../../../
# You must do the above commands again if you run `git clean -dfx`
```

#### OPTIONAL: edit platform files for local dev

##### add at the end of `src-cordova/platforms/ios/.....Classes/AppDelegate.m`

```swift
@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}
@end
```

### Android

#### You must prepare your Android dev environment with the correct PATH variables set in your .zshrc

#### EDIT FILES FOR LOCAL DEV (not sure if you have to do this really)

##### in file `src-cordova/platforms/android/CordovaLib/src/org/apache/cordova/engine/SystemWebViewClient.java`

##### in function: `public void onReceivedSslError`

``` java
// 1. COMMENT THIS LINE
// super.onReceivedSslError(view, handler, error);
// 2. ADD THESE TWO LINES
// ---->
handler.proceed();
return;
```
