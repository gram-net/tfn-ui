FROM chriswijnia/dev:latest

WORKDIR /var/tigerbee-app
COPY . /var/tigerbee-app

RUN yarn clean
RUN yarn setup 