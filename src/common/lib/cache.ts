const namespace = "tb.";

export function getCache<T = any>(key: string, defaultValue: T) {
  return JSON.parse(
    localStorage.getItem(namespace + key) || JSON.stringify(defaultValue)
  ) as T;
}

export function setCache(key: string, value: any = null) {
  localStorage.setItem(namespace + key, JSON.stringify(value));
}

export function getSession<T = any>(key: string, defaultValue: T) {
  return JSON.parse(
    sessionStorage.getItem(namespace + key) || JSON.stringify(defaultValue)
  ) as T;
}

export function setSession(key: string, value: any = null) {
  sessionStorage.setItem(namespace + key, JSON.stringify(value));
}
