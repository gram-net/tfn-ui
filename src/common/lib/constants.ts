export const nanoFactor = 1000000000;

export const fee = 0.052;

export const symbol = "🍑";

export const tfnCode = 13371337;

export const tonCode = 396;

export const gift = 1000;

export const sendTimeout = 40000;

export const forgeTimeout = 40000;
