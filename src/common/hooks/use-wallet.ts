import { reactive, ref, computed } from "@vue/composition-api";

import { TONClient, setWasmOptions } from "ton-client-web-js";

const initClient = async () => {
  // Adding an HTML update function
  console.log("init");
  (window as any).TONClient = (window as any).TONClient || TONClient;
  const addHTML = console.log;
  setWasmOptions({
    addHTML
  });
  // let createStart = Date.now();
  // creating a TONClient wit a net.ton.dev connection
  const client = await TONClient.create({
    servers: ["net.ton.dev"]
  });

  // // requesting TONClient creation time stamp
  // addHTML(`Client creation time: ${Date.now() - createStart}`);
  // // displaying the current client version
  // addHTML(`Client version: ${await client.config.getVersion()}`);
  // addHTML(`Client connected to: ${await client.config.data.servers}`);
  // const queryStart = Date.now();
  // // requesting top 10 accounts sorted by balance from net.ton.dev/graphql
  // const accounts = await client.queries.accounts.query(
  //   {},
  //   "id balance",
  //   [{ path: "balance", direction: "DESC" }],
  //   10
  // );
  // addHTML(`Query time: ${Date.now() - queryStart}`);
  // // displaying the data
  // addHTML(
  //   `<table>${accounts
  //     .map(x => `<tr><td>${x.id}</td><td>${BigInt(x.balance)}</td></tr>`)
  //     .join("")}</table>`
  // );
  // // displaying the data received time stamp
  // addHTML(`Now is: ${new Date()}`);
  return client;
};

export const useWallet = () => {
  const state = reactive({
    client: null as null | any,
    initialized: false
  });

  const init = async () => {
    state.client = await initClient();
    state.initialized = true;
    console.log(await getConfig());

    console.log(await getAccounts());
  };

  const getAccounts = async () => {
    const accounts = await state?.client.queries.accounts.query(
      {},
      "id balance",
      [{ path: "balance", direction: "DESC" }],
      10
    );

    return accounts;
  };

  const getConfig = async () => {
    const config = await state?.client.config.data.servers;
    console.log(config);
  };

  init();

  return {
    state,
    init,
    getAccounts
  };
};

export default useWallet;
