import axios from "axios";
import { GqlClient } from "./gql";
import gql from "graphql-tag";

export interface KeyPair {
  public: string;
  secret: string;
}

export interface NewWallet {
  createdIn: number;
  createdAt: number;
  account: string;
  keyPair: KeyPair;
  transactionId: string;
  balance: number;
  mnemonic: string;
}

export interface Mnemonic {
  mnemonic: string;
}

export interface SendOptions {
  value: number;
  keypair?: KeyPair;
  mnemonic?: string;
  address: string;
  recipient: string;
}

export interface Msg {
  id: string;
  msg_type: number;
  status: number;
  value: string;
  created_at: number;
  src: string;
  dst: string;
  status_name: string;
  msg_type_name: string;
  body: string;
}

export interface Tx {
  id: string;
  tr_type: number;
  total_fees: string;
  block_id: string;
  status: number;
  now: number;
  balance_delta_other: null | string;
  balance_delta: string;
  in_msg: string;
  out_messages: { id: string }[];
  status_name: string;
  tr_type_name: string;
}

export interface Account {
  acc_type: number;
  last_paid: number;
  last_trans_lt: string;
  balance: string;
}

const api = axios.create({
  baseURL: "https://freeton-api.herokuapp.com/"
});

const gqlTestClient = new GqlClient();
gqlTestClient.init("https://net.ton.dev/graphql");

const gqlMainClient = new GqlClient();
gqlMainClient.init("https://main.ton.dev/graphql");

const getGqlClient = (network = "") => {
  if (network.indexOf("net.ton.dev") > -1) return gqlTestClient;
  else if (network.indexOf("main.ton.dev") > -1) return gqlMainClient;
  else return gqlTestClient;
};

export const newWallet = async (mnemonic?: string, network?: string) => {
  const res = await api.get<NewWallet>("/newwallet", {
    params: {
      mnemonic,
      network
    }
  });

  return res.data;
};

export const mnemonic = async (network?: string) => {
  const res = await api.get<Mnemonic>("/mnemonic", {
    params: {
      network
    }
  });

  return res.data.mnemonic;
};

export const send = async (options: SendOptions, network?: string) => {
  const res = await api.post<{ id: string }>("/send", options, {
    params: {
      network
    }
  });
  return res.data.id;
};

export const getTxHistory = async (account: string, network?: string) => {
  const txQuery = gql`
    query tx($account: String!) {
      tx: transactions(
        filter: { account_addr: { eq: $account } }
        orderBy: { path: "created_at", direction: DESC }
        limit: 150
      ) {
        id
        tr_type

        total_fees
        block_id
        status
        now
        balance_delta_other {
          currency
          value
        }
        balance_delta
        in_msg
        out_messages {
          id
        }
        status_name
        tr_type
        tr_type_name
      }
    }
  `;
  const msgQuery = gql`
    query messages($account: String!) {
      messages(
        filter: { dst: { eq: $account } }
        orderBy: { path: "created_at", direction: ASC }
        limit: 150
      ) {
        id
        msg_type
        status
        value
        created_at
        src
        dst
        status_name
        msg_type
        msg_type_name
        body
      }
    }
  `;

  const [msgRes, txRes] = await Promise.all([
    getGqlClient(network).query<{ messages: Msg[] }>({
      query: msgQuery,
      fetchPolicy: "network",
      variables: {
        account
      }
    }),
    getGqlClient(network).query<{ tx: Tx[] }>({
      query: txQuery,
      fetchPolicy: "network",
      variables: {
        account
      }
    })
  ]);

  return {
    messages: msgRes?.messages || [],
    tx: txRes?.tx || []
  };
};

export const getTx = async (id: string, network?: string) => {
  const query = gql`
    query tx($id: String!) {
      tx: messages(
        filter: { id: { eq: $id } }
        orderBy: { path: "created_at", direction: DESC }
        limit: 1
      ) {
        id
        msg_type
        status
        value
        created_at
        src
        dst
        status_name
        msg_type
        msg_type_name
        body
      }
    }
  `;

  const res = await getGqlClient(network).query<{ messages: Tx[] }>({
    query,
    fetchPolicy: "network",
    variables: {
      id
    }
  });

  return (res?.messages || [])[0];
};

export const getAccount = async (account: string, network?: string) => {
  const query = gql`
    query accounts($account: String!) {
      accounts(filter: { id: { eq: $account } }) {
        acc_type
        last_paid
        last_trans_lt
        balance
      }
    }
  `;

  const res = await getGqlClient(network).query<{ accounts: Account[] }>({
    query,
    fetchPolicy: "network",
    variables: {
      account
    }
  });

  return (res?.accounts || [])[0];
};
