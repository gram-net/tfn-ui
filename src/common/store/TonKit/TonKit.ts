import { Module } from "vuex";
import { RootState } from "@/common/store";
import { ClientRemote as Client, Account } from "@tigerbee/tonkit";

export interface TonKitState {
  client: Client;
  account: typeof Account;
}

export const client = new Client();
export const account = Account;

const module = {
  state: {
    client,
    account
  },
  modules: {},
  namespaced: true
};

export default module;
