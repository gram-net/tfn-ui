import "./common/styles/app.scss";
import VuetifyDialog from "vuetify-dialog";
import "vuetify-dialog/dist/vuetify-dialog.css";
import vuedl from "vuedl";
import Vuetify from "vuetify";
import { vuetifyOptions } from "./vuetify";
import Vue2Filters from "vue2-filters";
import { Component, Vue } from "vue-property-decorator";
import VuetifyConfirm from "vuetify-confirm";
import VueCompositionAPI from "@vue/composition-api";

import _get from "lodash/get";
import App from "./App.vue";
import store from "./common/store";
import router from "./router";
import PortalVue from "portal-vue";
import { Help } from "./common/directives/help";
import VueTheMask from "vue-the-mask";

window.Vue = Vue as any;

export const vuetify = new Vuetify(vuetifyOptions);

Vue.prototype.$get = _get;
Vue.use(VueCompositionAPI);

Vue.use(VuetifyDialog, {
  context: {
    vuetify
  }
});

Vue.use(VueTheMask);
Vue.use(Vue2Filters);
Vue.use(PortalVue);
Vue.use(Vuetify);
Vue.use(VuetifyConfirm, { vuetify, color: "error", title: "Are you sure?" });
Vue.directive("help", Help);

@Component({
  store: store.original,
  router,
  vuetify,
  render: h => h(App)
})
export default class RootComponent extends Vue {}

const el = new RootComponent().$mount().$el;

export const confirm = Vue.prototype.$confirm as Vue["$confirm"];
export const dialog = Vue.prototype.$dialog as Vue["$dialog"];
(document.getElementById("app") as HTMLElement).appendChild(el);

import { TONClient, setWasmOptions } from "ton-client-web-js";

const addHTML = console.log;

window.addEventListener("load", () => {
  (async () => {
    // Adding an HTML update function
    setWasmOptions({
      addHTML
    });
    let createStart = Date.now();
    // creating a TONClient wit a net.ton.dev connection
    const client = await TONClient.create({
      servers: ["net.ton.dev"]
    });
    // requesting TONClient creation time stamp
    addHTML(`Client creation time: ${Date.now() - createStart}`);
    // displaying the current client version
    addHTML(`Client version: ${await client.config.getVersion()}`);
    addHTML(`Client connected to: ${await client.config.data.servers}`);
    const queryStart = Date.now();
    // requesting top 10 accounts sorted by balance from net.ton.dev/graphql
    const accounts = await client.queries.accounts.query(
      {},
      "id balance",
      [{ path: "balance", direction: "DESC" }],
      10
    );
    addHTML(`Query time: ${Date.now() - queryStart}`);
    // displaying the data
    addHTML(
      `<table>${accounts
        .map(x => `<tr><td>${x.id}</td><td>${BigInt(x.balance)}</td></tr>`)
        .join("")}</table>`
    );
    // displaying the data received time stamp
    addHTML(`Now is: ${new Date()}`);
  })();
});
