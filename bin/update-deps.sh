yarn stop-tfn
cd ../tonkit
git pull
cd ../tfn-core
git pull
cd ../tfn-api
git pull
cd ../tigerbee-ui
git pull
yarn upgrade @tigerbee/tonkit
echo ">> [update-deps] Pulled repos 'tonkit', 'tfn-core', 'tfn-api' and 'tigerbee-ui'"