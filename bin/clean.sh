echo ">> [clean] removing git untracked files and generated APK's"
git clean -fxd
rm -f ./src-cordova/app-release.apk
rm -f ./src-cordova/app-release-unsigned.apk
echo ">> [clean] finished"