cp .env.testnet .env
mkdir -p public/tapplets
yarn
yarn sync-config-xml
yarn update-tonkit --ignore-engines
yarn link @tigerbee/tonkit --ignore-engines
cd src-cordova
mkdir -p www
cordova platform add android
cordova platform add ios
yarn
cordova prepare