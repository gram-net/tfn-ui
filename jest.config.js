module.exports = {
  moduleFileExtensions: ["ts", "tsx", "js"],

  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },

  globals: {
    "ts-jest": {
      babelConfig: true
    }
  },

  setupFiles: ["<rootDir>/tests/unit/index.ts"],

  preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel"
};
