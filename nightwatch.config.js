const chromedriver = require("chromedriver");
const seleniumServer = require("selenium-server");
const path = require("path");

const electronBinPath = path.resolve(
  "./node_modules/electron/dist/Electron.app/Contents/MacOS/Electron"
);
const appPath = path.resolve("./dist_electron/index.js");

module.exports = {
  src_folders: ["tests/e2e/specs"],
  output_folder: "tests/e2e/reports",
  custom_assertions_path: ["tests/e2e/custom-assertions"],

  selenium: {
    start_process: true,
    server_path: seleniumServer.path,
    host: "127.0.0.1",
    port: 4444,
    cli_args: {
      "webdriver.chrome.driver": chromedriver.path
    }
  },

  test_settings: {
    default: {
      selenium_port: 4444,
      selenium_host: "localhost",
      silent: true
    },

    chrome: {
      desiredCapabilities: {
        browserName: "chrome",
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    },

    electron: {
      desiredCapabilities: {
        browserName: "chrome",
        javascriptEnabled: true,
        acceptSslCerts: true,
        chromeOptions: {
          binary: electronBinPath,
          args: [`app=${appPath}`]
        }
      }
    }
  }
};
